//[section comparison Query Operators]

//$gt/$gte operator
/*
	-it allows us to find documents that havw field number value greater than or equal to a specified value.
	Syntax:
	db.collectionName.find({field: {$gt : value}});
	db.collectionName.find({field: {$gte : value}});
*/

// $gt
	db.users.find({age: {$gt:50}}).pretty();

// $gte
	db.users.find({age: {$gte:21}}).pretty();

//$lt/$lte operator
	/*
		-it allows us to find/retrieve documents that have field number values less than or equal to a specified value

		Syntax:
		db.collectionName.find({field: {$lt : value}});
		db.collectionName.find({field: {$lte : value}});
	*/

// $lt
	db.users.find({age: {$lt:50}}).pretty();

// $lt
	db.users.find({age: {$lte:76}}).pretty();


// $ne operator
	/*
		-allows us to find documents that have field number values not equal to a specified value.

		Syntax:
		db.collectionName.find({field: {$ne: value}});

	*/

	db.users.find({age: {$ne: 68}});

//$in operator
	/*
		- it allows usto find documents/ocument with specific match criteria one field using different values.
		Syntax:
			db.collectionName.find({field: {$in: [value, valueB]}})
	*/

	db.users.find({lastName: {$in : ["Doe", "Hawking"]}});

	db.users.find({courses: {$in : ["HTML", "React"]}});

	db.users.find({courses: {$in : ["CSS", "JavaScript"]}});

//[section]
	//$or operator
	/*
		-allows us to find documents that match a single criteria from multiple provided search criteria.

		Syntax:
			db.collectionName.find({$or : [{fieldA: valueA}, {fieldB: valueB}]});
	*/

	db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

	db.users.find({$or: [{firstName: "Neil"}, {age: {$gt : 30}}]});

	//$and operator
	/*
		-allows us to find documents matching multiple provided criteria.
		Syntax:
			db.collectionName.find({$and : [{fieldA: valueA}, {fieldB: valueB}]});
	*/

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt : 30}}]});


	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt : 25}}]});


//[section] Field projection
	/*
		-retrieving documents are common operations that we do and by default mongoDb query return the whole documents as a response
	*/

	//inclusion
	/*
		-it allows us to include/ add specific fields only when retrieving documents.

		Syntax:
			db.users.find({criteria}, {fields: 1});
	*/

	db.users.find({firstName: "Jane"}, 
		{
			firstName: 1,
			lastName: 1,
			age:1
		});

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt : 30}}]},
		{
			firstName: 1,
			lastName: 1,
			age:1
		});


//masking data
	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt : 30}}]},
		{
			firstName: 1,
			lastName: "confidential",
			age:1
		});



//Exclusion
	/*
		-allows us to exclude/remove specific fields only when retrieving documents.
		-The value provided is 0.
	*/

	db.users.find({firstName: "Jane"}, {
		contact: 0,
		department: 0
	})


	db.users.find({$and: [{firstName: "Neil"}, {age: {$gte : 25}}]},
		{
			age:false;
			courses:0
		});

//Returning specific fields in Embedded documents

	//Inclusion
		db.users.find({firstName: "Jane"}, {
			"contact.phone": 1,
			
		})

	//Exclusion
		db.users.find({firstName: "Jane"}, {
			"contact.phone": 0,
			
		})

	//Inclusion
		db.users.find({firstName: "Jane"}, {
			"contact.phone": 1,
			_id:0			
		})

//[section] Evaluation Query Operators
	/*
		-itallows us to find documents that match a specific string patter using regular expressions
		Syntax:
			db.users.find({field: $regex : 'pattern', $options: '$optionValue' })
	
	*/

	//case sensitive query
	db.users.find({firstName: {$regex: 'N'}});

	//case insensitive query
	db.users.find({firstName: {$regex: 'N', $options: '$i'}});


	//CRUD operations

	//updateOne
	db.users.updateOne({age: {$lte : 17}},{
		$set : {
			firstName: "Chris",
			lastName: "Mortel",
		}
	});

	//deleteOne
	db.users.deleteOne({$and: [{firstName: "Chris"}, {lastName: "Mortel"}]})


	db.users.find({$and: [{age:{$lte : 82}}, {age:{$gte : 76}]})